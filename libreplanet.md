#  Gamifying Education the Libre Way




Gamification is a concept that has flourished in the digital age. By leveraging the high interactivity and enticing incentives of game design with tight feedback loops, difficult concepts can be absorbed with little effort. But can a complex and controversial topic such as climate change be transformed into a fun and exciting learning opportunity? In this talk, we will explore how the philosphy of free and open source software has been used to do exactly that by showing what we did to create and modify our own game and discuss how it can be applied in empowering educators and students to gamify their own interests.

##  Gamification



- Gamification is a process by which a topic or activity is represented as some kind of puzzle or game
  - Using principles of game design, players are educated about topics through play and interaction
  - This allows complex and even controversial topics to be experienced and discussed quite easily among a wide audience


##  The PHYLO(MON) Project



- The PHYLO(MON) Project is a crowd-sourced, open access, open development card game.
  - It was designed to teach real facts about nature and ecosystems in ways similar to fantasy card games like Magic or Pokemon
  - The creators heavily encourage the modifcation and extension of the game via the Creative Commons Attribution-Share Alike license
  - There are many different variants that exist, all created by other people and groups


##  Phylo: Western PA Edition



- We created a cooperative variant of the PHYLO(MON) game based on the ecosystems of Western PA
  - Players work together to build an ecosystem while encountering events related to or directly caused by human-led climate change
  - Players learn basic concepts of how ecosystems function and witness how devastating these climate change events can be


##  DocGen



- We created DocGen, a free and open source software which allows quick and easy creation of cards and other repetitive documents
  - We modified DocGen into a version exclusively for our Phylo card game
  - This version allows anyone to make a spreadsheet of cards and DocGen will instantly create a printable version of the game to play
  - We are working on a GUI version that allows people to input the card data in a form and see a preview of the card. They will be able to print and export their cards.

##  Education



- Through our use of free and open source software and Creative Commons licensing, we have empowered people to use the card game as a way to experience, learn about, and discuss climate change
- Our modified DocGen allows anyone to make their own deck and play the game with others, it is not just limited to Western PA.


