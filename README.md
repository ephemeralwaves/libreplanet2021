

<h1>
  Slides for LibrePlanet 2021
</h1>

## How to run
1. Run: git clone https://gitlab.com/ephemeralwaves/libreplanet2021.git
1. Then open index.html in any browser

## Created With
 reveal.js (available at [revealjs.com](https://revealjs.com).)




